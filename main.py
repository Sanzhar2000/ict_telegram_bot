import logging
from logging.handlers import RotatingFileHandler

from sqlalchemy.orm import Session
from aiogram import Bot, Dispatcher, executor, types

from config import settings
from database.database import SessionLocal
from database.models import Group

# Configure logging
# noinspection PyArgumentList
logging.basicConfig(
    handlers=[RotatingFileHandler("logs/log.log", maxBytes=10000000, backupCount=20)],
    format="%(asctime)s %(levelname)s:%(message)s",
    level=logging.INFO,
)

API_TOKEN = '2005812833:AAHt64hzk2QzJLheuw2ArmX-JxYDx3ee75o'

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    """
    This handler will be called when user sends `/start` or `/help` command
    """

    text1 = "Hello, welcome to our telegram bot created specifically for ICT endterm project.\n"
    text2 = "Creators of bot are:\n" + "   Sanzhar Toktassyn,\n" + "   Niyazbek Amantay,\n" + "   Danila Malyshko,\n" + "   Alimzhan Ospan\n"
    text3 = "Hope you will enjoy it!\n" + "\n"
    text4 = "Type /help in order to get brirf functionality of thee bot."

    await message.reply(text1 + text2 + text3 + text4)


@dp.message_handler(commands=['help'])
async def send_welcome(message: types.Message):
    """
    This handler will be called when user sends `/start` or `/help` command
    """
    await message.reply("/start - Start the Bot\n" +
                        "/services - to get University services\n" +
                        "Type the groupname in the following format in oder to get timetable: ST-2101\n")


@dp.message_handler(commands=['services'])
async def send_services(message: types.Message):
    response = settings.SERVICES

    await message.reply(response)


@dp.message_handler()
async def get_timetable_of_group(message: types.Message):
    db: Session = SessionLocal()
    group = db.query(Group).filter_by(title=message.text).first()
    if group:
        timetables = group.timetables
        if not timetables:
            await message.answer("No records available for this group")
        else:
            res = ""
            used_days = {}
            for tm in timetables:
                if used_days.get(tm.day.id) is None:
                    if len(used_days) != 0:
                        res += '\n'
                    res += f"{tm.day.title}:\n"
                    used_days[tm.day.id] = True
                res += f"{tm.time.title} {tm.course.title} {tm.classroom} {tm.type.title} {tm.instructor.name}\n"

            await message.answer(res)
    else:
        await message.reply("No such group or typed in not supported format")
        db.close()
        message.text = '/help'
        await send_welcome(message)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
