import os

from dotenv import find_dotenv, load_dotenv

load_dotenv(find_dotenv())


class Settings:
    PROJECT_NAME: str = "Kompra Filter"
    PROJECT_VERSION: str = "1.0.0"
    ROOT_PATH: str = os.getenv("ROOT_PATH", "")

    APP_ENV: str = os.getenv("APP_ENV", "production")
    APP_DEBUG: str = os.getenv("APP_DEBUG", "false")
    APP_LOG_LEVEL: str = os.getenv("APP_LOG_LEVEL", "info")

    POSTGRES_USER: str = os.getenv("POSTGRES_USER")
    POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")
    POSTGRES_SERVER: str = os.getenv("POSTGRES_SERVER", "localhost")
    POSTGRES_PORT: str = os.getenv("POSTGRES_PORT", 5432)
    POSTGRES_DB: str = os.getenv("POSTGRES_DB", "tdd")
    DATABASE_URL = f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_SERVER}:{POSTGRES_PORT}/{POSTGRES_DB}"

    SERVICES: str = """📌Основные службы поддержки студентов:

▫️Приемная ректора 
Телефон: (7172) 645732 
e-mail: info@astanait.edu.kz  

1️⃣ Деканат: 
Кабинет: №С1-322
Телефон: 8(7172)64-57-06 
e-mail: deans_office@astanait.edu.kz 
Осуществляет планирование, организацию, мониторинг, анализ и совершенствование учебной, учебно-методической и воспитательной работы; организацию учебного процесса на основе инновационных технологий и методов обучения, оценку учебных достижений в контексте студентоориентированного обучения, преподавания и оценки.

2️⃣ Студенческий отдел: 
Кабинет: № С1-270 
Телефон: 8(7172) 64-57-07 
e-mail:  madina.mukaliyeva@astanait.edu.kz 
Осуществляет формирование и ведение личных дел обучающихся на протяжении всего студенческого «жизненного цикла». Ведет учет контингента обучающихся по возрасту, полу, месту проживания (регион), национальности, условиях оплаты (по образовательному гранту, гранту МИО, гранту предприятий, организаций, на платной основе), социальному статусу.

3️⃣ Офис Регистратора университета: 
Кабинет: № C1-273 
Телефон: 8(7172) 64-57-07 
е-mail: aliya.koitanova@astanait.edu.kz 
Осуществляет регистрацию истории учебных достижений обучающихся на протяжении всего периода обучения. Сопровождает учебный процесс по кредитной и дистанционной образовательным технологиям.

4️⃣ Департамент социально-воспитательной работы: 
Кабинет: № С2-245 
Телефон: 8(7172) 64-57- 09 
e-mail: arman.kenzhebekov@astanait.edu.kz
Осуществляет создание информационно- образовательной и социокультурной среды, благоприятно влияющей на становление и развитие личности будущего специалиста. Занимается обеспечением условий для раскрытия и самореализации творческого потенциала студенческой молодежи, организацией студенческого досуга, поддержкой студенческих творческих и спортивных клубов и коллективов.

5️⃣ Департамент академической деятельности (ДАД): 
Кабинет: № С1-264 
Телефон: 8(7172)64-57-07
е-mail: ainur.bakenova@astanait.edu.kz 
Осуществляет планирование, организацию и контроль учебного процесса университета, внедрение инновационных технологий обучения.

6️⃣ Департамент науки и инноваций: 
Кабинет: № С2-155 
Телефон: 8(7172)64-57-13 
e-mail: Sholpan.Borashova@astanait.edu.kz 
Департамент осуществляет планирование, координацию и организацию научной и инновационной деятельности; развитие перспективных форм сотрудничества, в том числе международного, с IT-компаниями и отраслями экономики для совместного решения научных задач и использования научных разработок.

7️⃣ Департамент маркетинга и связи с общественностью: 
Кабинет: № С2-368 
Телефон: 8(7172) 64-57-18 
e-mail: Dastan.demeugazyyev@astanait.edu.kz
Разрабатывает маркетинговую стратегию. Осуществляет взаимодействие со средствами массовой информации, общественными организациями для своевременного информирования общественности о важнейших событиях в деятельности Astana IT University.

8️⃣ Общее руководство службой поддержки студентов осуществляют:
Омирбаев Серик Мауленович, доктор экономических наук, профессор, 
Тел: 8(7172)64- 57-22 
e-mail: serik.omirbayev@astanait.edu.kz"""

settings = Settings()

if not os.path.exists("logs"):
    os.makedirs("logs")
