from sqlalchemy.orm import Session

from database.database import SessionLocal
from database.models import Group


def start():
    db: Session = SessionLocal()
    group = db.query(Group).filter_by(title="ST-2101").first()
    if group:
        timetables = group.timetables
        for tm in timetables:
            print(f"{tm.day.title} {tm.time.title} {tm.course.title} {tm.classroom} {tm.type.title} {tm.instructor.name}")

    db.close()
    return True


if __name__ == '__main__':
    start()