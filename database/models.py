import datetime

from sqlalchemy import (JSON, TIMESTAMP, Boolean, Column, DateTime, Float,
                        ForeignKey, Integer, String, Text)
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from database.database import Base


class Group(Base):
    __tablename__ = "groups"

    id = Column(Integer, primary_key=True, index=True)

    title = Column(String)

    timetables = relationship("Timetable", back_populates="group")


class Day(Base):
    __tablename__ = "days"

    id = Column(Integer, primary_key=True, index=True)

    title = Column(String)

    timetables = relationship("Timetable", back_populates="day")


class Time(Base):
    __tablename__ = "times"

    id = Column(Integer, primary_key=True, index=True)

    title = Column(String)

    timetables = relationship("Timetable", back_populates="time")


class Course(Base):
    __tablename__ = "courses"

    id = Column(Integer, primary_key=True, index=True)

    title = Column(String)

    timetables = relationship("Timetable", back_populates="course")


class Type(Base):
    __tablename__ = "types"

    id = Column(Integer, primary_key=True, index=True)

    title = Column(String)

    timetables = relationship("Timetable", back_populates="type")


class Instructor(Base):
    __tablename__ = "instructors"

    id = Column(Integer, primary_key=True, index=True)

    name = Column(String)

    timetables = relationship("Timetable", back_populates="instructor")


class Timetable(Base):
    __tablename__ = "timetables"

    id = Column(Integer, primary_key=True, index=True)

    group_id = Column(ForeignKey("groups.id"))
    day_id = Column(ForeignKey("days.id"))
    time_id = Column(ForeignKey("times.id"))
    course_id = Column(ForeignKey("courses.id"))
    classroom = Column(String)
    type_id = Column(ForeignKey("types.id"))
    instructor_id = Column(ForeignKey("instructors.id"))

    group = relationship("Group", back_populates="timetables")
    day = relationship("Day", back_populates="timetables")
    time = relationship("Time", back_populates="timetables")
    course = relationship("Course", back_populates="timetables")
    type = relationship("Type", back_populates="timetables")
    instructor = relationship("Instructor", back_populates="timetables")

# class Company(Base):
#     __tablename__ = "company"
#
#     id = Column(Integer, primary_key=True, index=True)
#
#     iin = Column(String)
#     full_name = Column(String)
#     name = Column(String)
#
#     created = Column(DateTime(timezone=True), server_default=func.now())
#     last_updated = Column(DateTime(timezone=True), onupdate=func.now())
#
#     bin = Column(String, unique=True)
#     fact_address = Column(Text)
#     field = Column(Text)
#     law_address = Column(Text)
#     ip = Column(Boolean)
#
#     oked = Column(String, ForeignKey("oked.oked"))
#     okpo = Column(String)
#     kato = Column(String)
#     workers = Column(String)
#     region = Column(String)
#     register_date = Column(DateTime(timezone=True))
#     active = Column(Boolean)
#     unreliable = Column(Boolean)
#     bad_supplier = Column(Boolean)
#     size = Column(String)
#     ownership = Column(String)
#
#     contacts = relationship("Contact", back_populates="company", lazy="joined")
#     filter_selectors = relationship(
#         "FilterSelector",
#         secondary="filter_selector_company",
#         back_populates="companies",
#     )
#     tax = relationship("Tax", back_populates="company")
#     filter_taxes = relationship("FilterTax", back_populates="company")
#     sale = relationship("Sale", back_populates="company")
#     owner = relationship("Owner", back_populates="companies", uselist=False, lazy="joined")
#
#
# class Owner(Base):
#     __tablename__ = "owner"
#
#     id = Column(Integer, primary_key=True, index=True)
#     created = Column(DateTime(timezone=True))
#     last_updated = Column(DateTime(timezone=True))
#     company = Column(ForeignKey("company.id"))
#     name = Column(String)
#     iin = Column(String)
#     birthday = Column(String)
#     appointment_date = Column(DateTime(timezone=True))
#
#     companies = relationship("Company", back_populates="owner")
#
#
# class Oked(Base):
#     __tablename__ = "oked"
#
#     oked = Column(String, primary_key=True)
#     name = Column(Text, index=True)
#     parent_oked = Column(String)
#
#
# class Kato(Base):
#     __tablename__ = "kato"
#
#     id = Column(Integer, primary_key=True, index=True)
#     name_ru = Column(String)
#     name_kz = Column(String)
#     code = Column(Integer)
#     parent_id = Column(Integer)
#     branch = Column(Boolean)
#
#
# class Contact(Base):
#     __tablename__ = "contacts"
#
#     id = Column(Integer, primary_key=True, index=True)
#     iinbin = Column(String, unique=True)
#     phones = Column(String, unique=True)
#     email = Column(String, unique=True)
#     website = Column(String)
#     fact_address = Column(String)
#     ip = Column(Boolean)
#     source = Column(String)
#     name = Column(String)
#     company_id = Column(Integer, ForeignKey("company.id"))
#
#     company = relationship("Company", back_populates="contacts")
#
#
# class User(Base):
#     __tablename__ = "users"
#
#     id = Column(Integer, primary_key=True, index=True)
#     balance = Column(Float)
#     username = Column(String)
#     password = Column(String)
#
#     filter_selectors = relationship(
#         "FilterSelector", order_by="desc(FilterSelector.created)", back_populates="user"
#     )
#     access_limit = relationship("AccessLimit", back_populates="user")
#     access_limit_stat = relationship("AccessLimitStat", back_populates="user")
#     sale = relationship("Sale", back_populates="user")
#
#
# class FilterSelector(Base):
#     __tablename__ = "filter_selector"
#
#     id = Column(Integer, primary_key=True, index=True)
#     user_id = Column(ForeignKey("users.id"))
#     title = Column(String)
#     filter = Column(JSON)
#     contacts = Column(Boolean)
#     xlsx_file = Column(String)
#     xlsx_key = Column(String)
#     count = Column(Integer)
#     status = Column(String)
#     favorite = Column(Boolean)
#     price = Column(Integer)
#     created = Column(TIMESTAMP, default=datetime.datetime.now())
#
#     user = relationship("User", back_populates="filter_selectors")
#     companies = relationship(
#         "Company",
#         secondary="filter_selector_company",
#         back_populates="filter_selectors",
#     )
#
#
# class FilterSelectorCompany(Base):
#     __tablename__ = "filter_selector_company"
#
#     filter_selector_id = Column(
#         Integer, ForeignKey("filter_selector.id"), primary_key=True
#     )
#     company_id = Column(Integer, ForeignKey("company.id"), primary_key=True)
#
#
# class AccessLimit(Base):
#     __tablename__ = "access_limits"
#
#     id = Column(Integer, primary_key=True, index=True)
#     user_id = Column(ForeignKey("users.id"))
#     access_right_id = Column(ForeignKey("f_access_rights.id"))
#     period_type_id = Column(Integer)
#     left_count = Column(Integer)
#     total_count = Column(Integer)
#     date_from = Column(DateTime(timezone=True))
#     date_to = Column(DateTime(timezone=True))
#
#     user = relationship("User", back_populates="access_limit")
#     access_right = relationship("AccessRight", back_populates="access_limit")
#
#
# class AccessLimitStat(Base):
#     __tablename__ = "access_limits_stat"
#
#     id = Column(Integer, primary_key=True, index=True)
#     resource_id = Column(ForeignKey("f_access_rights.id"))
#     user_id = Column(ForeignKey("users.id"))
#     company_id = Column(ForeignKey("company.id"))
#     full_url = Column(String)
#     response_status = Column(Integer)
#     response = Column(Text)
#     client_ip = Column(String)
#     created = Column(DateTime(timezone=True))
#     last_updated = Column(DateTime(timezone=True))
#
#     access_right = relationship("AccessRight", back_populates="access_limit_stat")
#     user = relationship("User", back_populates="access_limit_stat")
#
#
# class AccessRight(Base):
#     __tablename__ = "f_access_rights"
#
#     id = Column(Integer, primary_key=True, index=True)
#     name_ru = Column(String)
#
#     access_limit = relationship("AccessLimit", back_populates="access_right")
#     access_limit_stat = relationship("AccessLimitStat", back_populates="access_right")
#
#
# class Tax(Base):
#     __tablename__ = "tax"
#
#     id = Column(Integer, primary_key=True, index=True)
#
#     amount = Column(Float)
#     year = Column(Integer)
#     company_id = Column(Integer, ForeignKey("company.id"), primary_key=True)
#     nds = Column(Float)
#     bin = Column(Text)
#     company = relationship("Company", back_populates="tax")
#
#
# class Sale(Base):
#     __tablename__ = "sale"
#
#     id = Column(Integer, primary_key=True, index=True, autoincrement=True)
#
#     company_id = Column(
#         Integer, ForeignKey("company.id"), primary_key=True, nullable=True
#     )
#     date = Column(DateTime(timezone=True))
#     payment = Column(Float, default=0)
#     user_id = Column(Integer, ForeignKey("users.id"), primary_key=True, nullable=True)
#     company_name = Column(String)
#     created = Column(DateTime(timezone=True))
#     last_updated = Column(DateTime(timezone=True))
#     scheme = Column(Boolean)
#     inn = Column(String)
#     history = Column(Boolean)
#     refund = Column(Boolean)
#     sale = Column(String)
#     country = Column(String)
#     podbor_id = Column(Integer)
#     podbor_name = Column(String)
#     filter = Column(Boolean)
#
#     company = relationship("Company", back_populates="sale")
#     user = relationship("User", back_populates="sale")
#
#
# class FilterTax(Base):
#     __tablename__ = "filter_taxes"
#
#     id = Column(Integer, primary_key=True, index=True)
#     identifier = Column(String)
#     company_id = Column(Integer, ForeignKey("company.id"), primary_key=True)
#
#     y_2015 = Column(Float, default=0)
#     y_2016 = Column(Float, default=0)
#     y_2017 = Column(Float, default=0)
#     y_2018 = Column(Float, default=0)
#     y_2019 = Column(Float, default=0)
#     y_2020 = Column(Float, default=0)
#     y_2021 = Column(Float, default=0)
#     y_2022 = Column(Float, default=0)
#     y_2023 = Column(Float, default=0)
#     y_2024 = Column(Float, default=0)
#     y_2025 = Column(Float, default=0)
#
#     company = relationship("Company", back_populates="filter_taxes")
